var actualCode = `
async function sendSolution() {
    var output = {
        token: CaptchaText(),
        gid: $J('#captchagid').val()
    }
    if (window.location.hash !== "#accgen")
    {
        var url = new URL(document.referrer);
        window.parent.postMessage(JSON.stringify(output), url.hostname.endsWith("cathook.club") ? url.origin : "https://accgen.cathook.club"); //get recaptcha token and send to accgen
    }
    else
        window.postMessage({
            accgen: true,
            data: JSON.stringify(output)
        }, "*");
}

var sendSolutionInterval = setInterval(function() { if (CaptchaText()) { sendSolution(); clearInterval(sendSolutionInterval); }}, 500);
var iframe = window.location.hash !== "#accgen";

$J('#email').val('do_not_edit_this@do_not_change.this').parent().parent().toggle(iframe);
$J('#reenter_email').val('do_not_edit_this@do_not_change.this').parent().parent().toggle(iframe);
$J('#i_agree_check').prop('checked', true).parent().parent().toggle(iframe);
if (!iframe) {
    $J('#country').parent().parent().parent().hide();
    $J('#createAccountButton').parent().parent().hide();
    $J("#global_header").hide();
    $J("#footer").hide();
} else {
    $J('body').removeClass();
    // Hide stuff we don't need
    $J('body').children().hide();
    // Move captcha entry to the beginning of the page
    $J("#captcha_entry").prependTo("body");
    // Center it
    $J("#captcha_entry").css({
        "width": "100%",
        "text-align": "center"
    });
    $J("#captcha_entry_recaptcha").css({
       "display": "inline-block"
    });
}
`

function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

window.addEventListener("message", function (event) {
    if (event.source == window &&
        event.data &&
        event.data.accgen) {
        console.log(event.data.data);
        chrome.runtime.sendMessage({ task: "nativeCaptcha", data: event.data.data });
        window.close();
    }
});

// We don't want to interfer with other account generator extensions, let's make sure we dont
var isIframed = (inIframe() && new URL(document.referrer).hostname.endsWith("cathook.club"));
var isPopup = !isIframed && window.location.hash === "#accgen"

if (isIframed || isPopup) {
    var e = document.createElement("script");
    e.textContent = actualCode;
    (document.head || document.documentElement).appendChild(e);

    var interval = setTimeout(() => {
        // We have detected that someone is messing with our window
        // If the developer of the "Project Centurion AG" extension is reading this,
        // please stop messing with join pages not created by your extension.
        // This code will stop triggering automatically.
        if (!document.getElementById("captcha_entry") && window.confirm("Interference by another extension has been detected, would you like to disable it?"))
            // Ask the background script to disable any other extensions. After we are done, reload the window.
            chrome.runtime.sendMessage({ task: "disableOtherExtensions" }, undefined, (out) => { if (out == true) window.location.reload(); });
    }, 1000);
}