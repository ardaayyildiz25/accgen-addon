var browser_api = typeof browser == "undefined" ? chrome : browser;

var port = browser_api.runtime.connect({ name: "port-from-cs" });

document.addEventListener('addon', function (event) {
    port.postMessage(event.detail);
});
port.onMessage.addListener(function (reply) {
    var event = new CustomEvent('addon_reply', { detail: JSON.stringify(reply) });
    document.dispatchEvent(event)
});

/*var g_id = 0;
function messageAddon(data) {
    var id = g_id++;
    document.dispatchEvent(new CustomEvent("addon", { "detail": { id: id, data: data } }));
    return new Promise(function (resolve, reject) {
        document.addEventListener("addon_reply", function (event) {
            resolve(JSON.parse(event.detail).data);
        });
    });
}

await messageAddon({ task: "version" })*/