const gmailpermissions = {
    origins: ["https://mail.google.com/*", "https://mail-attachment.googleusercontent.com/*"]
};

var browser_api = typeof browser == "undefined" ? chrome : browser;

function pressed() {
    browser_api.permissions.request(gmailpermissions, function (granted) {
        if (granted) {
            var browser_api = chrome ? chrome : browser;
            browser_api.windows.remove(browser_api.windows.WINDOW_ID_CURRENT);
        }
    });
}
window.onload = function () {
    document.getElementById("grant").addEventListener("click", pressed);
}
